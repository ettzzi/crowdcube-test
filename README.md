This project aims to deliver my own implementation of: https://www.crowdcube.com/investments?

The time was restricted to 2.5 hours

## Requirements 
Node and npm or yarn

## Setup
Clone the repository and then run `npm install` or `yarn` inside the project folder

## Run 
`yarn start` or `npm run start`

## Questions

*How performant is my solution? Can performance be improved?*
Yes! 
- Optimizing the critical rendering path;
- Optimizing images for different devices;
- Server-side rendering;
- Cache content (Service Workers)
- Remove style rules not used
- Use async for the scripts

*How can the front-end be tested?*
Since it's built with React we can use Jest. We can also build some e2e testing with Selenium Webdriver

*How can we make sure the page is accessible?*
To be sure maybe it's better to do some user testing with people with various disabilities or hire a company that will check if the accessibility guidelines from W3C are respected.

As developers some easy things we could do are:
- Use semantic markup (header, main, article etc...) and combine them with role attribute
- Have alt text 
- Add a skip to content
- Provide another version for users without JS enabled
- Use label for the input, select, radio and possibly use native HTML elements
- Use colours that have enough colour contrast between each other


*How will this page look on different devices?*
The page is not responsive and I've not used also any autoprefixer for CSS rules. 
So, I suppose it will be broken for some devices. 

*How will the page handle large numbers of opportunities?*
In the current Crowdcube website, there is an infinite-scroll.
Another way to do it is with pagination coming from the API.

*How will the page handle a small/zero number of opportunities?*
Not showing anything with 0. Ideally, we should have a message for the user. 
If pagination is used we can disable the next/previous buttons.

*How can we build the page so it can be adaptable to change/new features?*
Ideally, we should have components that are independent from each other. This will allow us to inject/remove a new component e

*How could the page be more dynamic/engaging?*
Some animation on the star at the top right of the card when hovered. Having a clear CTA inside the cards. Have different weights/sizes for different investment opportunities.