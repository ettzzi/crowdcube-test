import React from 'react';
import NavLink from './NavLink/NavLink';
import NavButton from './NavButton/NavButton';

const NavGroup = () => (
  <div>
  <NavButton text="Investment opportunities" outline={true} />
  <NavLink
    text="How it works"
    url="https://www.crowdcube.com/investing-your-money"
  />
  <NavLink text="Explore" url="https://www.crowdcube.com/explore/" />
  <NavLink text="Log in" url="https://www.crowdcube.com/login/" />
  <NavButton text="Join" />
  </div>
);

export default NavGroup;
