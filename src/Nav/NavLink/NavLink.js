import React from 'react';
import './NavLink.css';

const NavLink = (props) => (
  <a className='navLink' href={props.url}>{props.text}</a>
)

export default NavLink;
