import React from 'react'; 
import './NavButton.css';

const NavButton = (props) => {
  let className = 'navButton--primary'
  if (props.outline) {
    className = 'navButton--outline'
  }
  return (
    <a href={props.url} className={`navButton ${className}`}>{props.text}</a>    
  )
}

export default NavButton;
