import React from 'react';
import NavGroup from './NavGroup';
import Logo from '../Logo/Logo.js';

import './Nav.css';

const Nav = () => (
  <div className="Nav">
    <Logo />
    <div>
      <NavGroup />  
    </div>
  </div>
)

export default Nav;
