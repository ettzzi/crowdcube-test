import React from 'react';
import './Stats.css';

export default props => (
  <div className="Stats">
    <div className="Stats__group">
      <p className="Stats__title">Raised</p>
      <p className="Stats__value">£158,830</p>
    </div>
    <div className="Stats__group">
      <p className="Stats__title">Equity</p>
      <p className="Stats__value">20.93%</p>
    </div>
    <div className="Stats__group">
      <p className="Stats__title">Investors</p>
      <p className="Stats__value">252</p>
    </div>
  </div>
);

