import React from 'react';
import './Progress.css';

export default (props) => {
  const styles = {
    width: props.percentage + '%'
  };
  return (
    <div className="ProgressBar">
      <div className="ProgressBar__container">
        <span className="ProgressBar__value">{props.percentage + '%'}</span>
        <div className="ProgressBar__bar" style={styles} />
      </div>
    </div>
  )
}
