import React from 'react'; 
import logo from './logo.svg';
import './Logo.css';

export default () => <a href="/" className="Logo"><img src={logo} alt="Crowdcube"/></a>
