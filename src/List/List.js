import React from 'react';
import Card from '../Card/Card';

const style = {
  display: 'flex',
  flexWrap: 'wrap'
};
const List = (props) => (
  <div style={style}>
    {props.items.map(item => <Card key={item.id} {...item} />)}
  </div>
)

export default List;
